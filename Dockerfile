ARG CMSOS_IMAGE
FROM ${CMSOS_IMAGE}
ARG CMSOS_IMAGE
RUN echo "CMSOS_IMAGE="${CMSOS_IMAGE}
# add application software
#RUN dnf -y remove cmsos-core-metris cmsos-core-metris-devel
#RUN dnf -y remove cmsos-core-topics cmsos-core-topics-devel
#RUN dnf -y remove cmsos-core-speedial cmsos-core-speedial-devel
ADD rpm /tmp/rpm
RUN dnf -y install /tmp/rpm/*.rpm
RUN rm -rf /tmp/rpm
#set XDAQ environment
ENV XDAQ_SETUP_ROOT /opt/xdaq/share
ENV XDAQ_DOCUMENT_ROOT /opt/xdaq/htdocs
ENV LD_LIBRARY_PATH /opt/xdaq/lib:/opt/xdaq/lib64
ENV XDAQ_ROOT /opt/xdaq
ENV XDAQ_DOCUMENT_ROOT /opt/xdaq/htodocs

# entry poiny
ENTRYPOINT ["/opt/xdaq/bin/xdaq"]
CMD ["-l", "INFO"]
