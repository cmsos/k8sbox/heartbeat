set CLUSTER_NAME="k8sbox"

setenv KUBECONFIG $HOME/cern/openstack/k8sbox/config

#helm

helm repo add heartbeat-devel https://gitlab.cern.ch/api/v4/projects/156634/packages/helm/devel

helm repo update

helm search repo --devel

helm uninstall heartbeat

helm install heartbeat heartbeat-devel/cmsos-heartbeat-helm --devel

#kubectl 

kubectl get pods -n heartbeat

kubectl get services -n heartbeat -o wide

kubectl exec -ti <podid> -n heartbeat -- bash

#browse with manual proxy at localhost 1082

ssh -ND 1082 lxplus.cern.ch

http://k8sbox-bnx7wmqhhrjl-node-2.cern.ch:32000/urn:xdaq-application:lid=50
